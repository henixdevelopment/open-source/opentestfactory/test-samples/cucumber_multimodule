This is an example of a Cucumber multimodule project. 

There is a parent pom.xml (located in the root directory of the project ) and there are 3 "children" pom.xml located in each folder (Casino, JpetStore, Katalon). 

The tests in the folder "Casino" should be returned as "Success" in Squash TM
The tests in the folder "JpetStore" should be returned as "Failed" in Squash TM
